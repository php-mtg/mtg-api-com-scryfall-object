# php-mtg/mtg-api-com-scryfall-object

A library that implements the php-mtg/mtg-api-com-scryfall-interface.

![coverage](https://gitlab.com/php-mtg/mtg-api-com-scryfall-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-mtg/mtg-api-com-scryfall-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-mtg/mtg-api-com-scryfall-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpMtg\Scryfall\ScryfallApiEndpoint;

/* @var $client \Psr\Http\Client\ClientInterface */

$endpoint = new ScryfallApiEndpoint($client);

$collection = $endpoint->getSets();

foreach($collection as $set)
{
	$page = 1;
	do
	{
		/** @var \PhpMtg\Scryfall\ScryfallApiPagination $pagination */
		$pagination = $endpoint->getCardsSearch($page, 'e='.$set->getId());
		
		foreach($pagination->getData() as $card)
		{
			// do something with $card
		}
		
		$page++;
	}
	while($pagination->hasMore());
}

```


## License

MIT (See [license file](LICENSE)).
