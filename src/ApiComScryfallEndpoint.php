<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use InvalidArgumentException;
use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Stringable;

/**
 * ApiComScryfallEndpoint class file.
 *
 * This class is the entry point for all methods using this api.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiComScryfallEndpoint implements ApiComScryfallEndpointInterface
{
	
	public const HOST = 'https://api.scryfall.com/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * Constructor of the instance. This object acts as a coordinator between
	 * all the modules of the mioga application.
	 *
	 * This method builds the object but does not connect to it, so it never
	 * throws exceptions.
	 *
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 * @throws InvalidArgumentException
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$configuration = $this->_reifier->getConfiguration();
		
		$configuration->addIgnoreExcessFields(ApiComScryfallPagination::class, ['object']);
		$configuration->addIgnoreExcessFields(ApiComScryfallExtension::class, ['object']);
		$configuration->addIgnoreExcessFields(ApiComScryfallCatalog::class, ['object']);
		$configuration->addIgnoreExcessFields(ApiComScryfallCard::class, ['object']);
		$configuration->addIgnoreExcessFields(ApiComScryfallRuling::class, ['object']);
		$configuration->addIgnoreExcessFields(ApiComScryfallParsedMana::class, ['object']);
		$configuration->addIgnoreExcessFields(ApiComScryfallRelatedCard::class, ['object']);
		$configuration->addIgnoreExcessFields(ApiComScryfallCardFace::class, ['object']);
		
		$configuration->setIterableInnerTypes(ApiComScryfallPagination::class, ['data'], Stringable::class);
		$configuration->addPolymorphism(Stringable::class, 0, 'object', 'set', ApiComScryfallExtension::class);
		$configuration->addPolymorphism(Stringable::class, 0, 'object', 'card_symbol', ApiComScryfallSymbol::class);
		$configuration->addPolymorphism(Stringable::class, 0, 'object', 'ruling', ApiComScryfallRuling::class);
		$configuration->addPolymorphism(Stringable::class, 0, 'object', 'card', ApiComScryfallCard::class);
		
		$configuration->setIterableInnerTypes(ApiComScryfallCard::class, ['multiverse_ids'], 'int');
		$configuration->setIterableInnerTypes(ApiComScryfallCard::class, ['all_parts'], ApiComScryfallRelatedCard::class);
		$configuration->setIterableInnerTypes(ApiComScryfallCard::class, ['card_faces'], ApiComScryfallCardFace::class);
		$configuration->setIterableInnerTypes(ApiComScryfallCard::class, ['colors', 'color_identity', 'color_indicator', 'artist_ids', 'finishes', 'frame_effects', 'games', 'produced_mana', 'promo_types', 'keywords'], 'string');
		$configuration->setIterableInnerTypes(ApiComScryfallCardFace::class, ['color_indicator', 'colors', 'image_uris'], 'string');
		$configuration->setIterableInnerTypes(ApiComScryfallCatalog::class, ['data'], 'string');
		$configuration->setIterableInnerTypes(ApiComScryfallParsedMana::class, ['colors'], 'string');
		$configuration->setIterableInnerTypes(ApiComScryfallSymbol::class, ['colors', 'gatherer_alternates'], 'string');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getSets()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getSets() : ApiComScryfallPagination
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'sets');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		/** @psalm-suppress LessSpecificReturnStatement */
		return $this->_reifier->reify(ApiComScryfallPagination::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getSetByCode()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetByCode(string $setCode) : ApiComScryfallExtension
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'sets/'.\urlencode($setCode));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallExtension::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getSetByTcgplayerId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetByTcgplayerId(int $groupId) : ApiComScryfallExtension
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'sets/tcgplayer/'.\urlencode((string) $groupId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallExtension::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getSetByScryfallId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetByScryfallId(UuidInterface $setId) : ApiComScryfallExtension
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'sets/'.$setId->__toString());
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallExtension::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardsSearch()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getCardsSearch(int $page = 1, ?string $query = null, ?string $unique = null, ?string $order = null, ?string $dir = null, bool $includeExtras = false, bool $includeMultilingual = false, bool $includeVariations = false) : ApiComScryfallPagination
	{
		$url = self::HOST.'cards/search?page='.((string) $page).'&q='.\urlencode((string) $query);
		if(null !== $unique)
		{
			$url .= '&unique='.\urlencode($unique);
		}
		if(null !== $order)
		{
			$url .= '&order='.\urlencode($order);
		}
		if(null !== $dir)
		{
			$url .= '&dir='.\urlencode($dir);
		}
		if($includeExtras)
		{
			$url .= '&include_extras';
		}
		if($includeMultilingual)
		{
			$url .= '&include_multilingual';
		}
		if($includeVariations)
		{
			$url .= '&include_variations';
		}
		
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		/** @psalm-suppress LessSpecificReturnStatement */
		return $this->_reifier->reify(ApiComScryfallPagination::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardNamed()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardNamed(string $name, bool $fuzzy = false, ?string $set = null) : ?ApiComScryfallCard
	{
		$url = self::HOST.'cards/named?';
		$url .= ($fuzzy ? 'fuzzy' : 'exact').'='.\urlencode($name);
		if(null !== $set)
		{
			$url .= '&set='.\urlencode($set);
		}
		
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyNullable(ApiComScryfallCard::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardsAutocomplete()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardsAutocomplete(string $query, bool $includeExtras = false) : ApiComScryfallCatalog
	{
		$url = self::HOST.'cards/autocomplete?q='.\urlencode($query);
		if($includeExtras)
		{
			$url .= '&include_extras';
		}
		
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardRandom()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardRandom(?string $query = null) : ApiComScryfallCard
	{
		$url = self::HOST.'cards/random';
		if(null !== $query)
		{
			$url .= '?q='.\urlencode($query);
		}
		
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCard::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCard()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCard(string $setCode, string $collNb, ?string $lang = null) : ApiComScryfallCard
	{
		$url = self::HOST.'cards/'.\urlencode($setCode).'/'.\urlencode($collNb);
		if(null !== $lang)
		{
			$url .= '/'.\urlencode($lang);
		}
		
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCard::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardByMultiverseId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByMultiverseId(int $multiverseId) : ApiComScryfallCard
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/multiverse/'.((string) $multiverseId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCard::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardByMtgoId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByMtgoId(int $mtgoId) : ApiComScryfallCard
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/mtgo/'.((string) $mtgoId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCard::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardByArenaId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByArenaId(int $arenaId) : ApiComScryfallCard
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/arena/'.((string) $arenaId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCard::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardByTcgplayerId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByTcgplayerId(int $tcgpId) : ApiComScryfallCard
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/tcgplayer/'.((string) $tcgpId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCard::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardByCardmarketId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByCardmarketId(int $mcmId) : ApiComScryfallCard
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/cardmarket/'.((string) $mcmId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCard::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardByScryfallId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByScryfallId(UuidInterface $uuid) : ApiComScryfallCard
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/'.$uuid->__toString());
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCard::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardImage()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ParseThrowable
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardImage(UuidInterface $cardId, string $format = 'large', bool $back = false) : string
	{
		$url = self::HOST.'cards/'.$cardId->__toString().'?format=image&version='.\urlencode($format);
		if($back)
		{
			$url .= '&face=back';
		}
		
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		
		return $this->_httpClient->sendRequest($request)->getBody()->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getRulingsFromMultiverseId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromMultiverseId(int $multiverseId) : ApiComScryfallPagination
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/multiverse/'.((string) $multiverseId).'/rulings');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		/** @psalm-suppress LessSpecificReturnStatement */
		return $this->_reifier->reify(ApiComScryfallPagination::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getRulingsFromMtgoId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromMtgoId(int $mtgoId) : ApiComScryfallPagination
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/mtgo/'.((string) $mtgoId).'/rulings');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		/** @psalm-suppress LessSpecificReturnStatement */
		return $this->_reifier->reify(ApiComScryfallPagination::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getRulingsFromArenaId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromArenaId(int $arenaId) : ApiComScryfallPagination
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/arena/'.((string) $arenaId).'/rulings');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		/** @psalm-suppress LessSpecificReturnStatement */
		return $this->_reifier->reify(ApiComScryfallPagination::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getRulingsFromCard()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromCard(string $extCode, string $cardNb) : ApiComScryfallPagination
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/'.\urlencode($extCode).'/'.\urlencode($cardNb).'/rulings');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		/** @psalm-suppress LessSpecificReturnStatement */
		return $this->_reifier->reify(ApiComScryfallPagination::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getRulingsFromScryfallId()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromScryfallId(UuidInterface $scryfallCardId) : ApiComScryfallPagination
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'cards/'.$scryfallCardId->__toString().'/rulings');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		/** @psalm-suppress LessSpecificReturnStatement */
		return $this->_reifier->reify(ApiComScryfallPagination::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getSymbology()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getSymbology() : ApiComScryfallPagination
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'symbology');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		/** @psalm-suppress LessSpecificReturnStatement */
		return $this->_reifier->reify(ApiComScryfallPagination::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getParsedMana()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getParsedMana(string $cost) : ApiComScryfallParsedMana
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'symbology/parse-mana?cost='.\urlencode($cost));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallParsedMana::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCardNames()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardNames() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/card-names');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getArtistNames()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getArtistNames() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/artist-names');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getWordBank()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getWordBank() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/word-bank');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getCreatureTypes()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCreatureTypes() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/creature-types');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getPlaneswalkerTypes()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPlaneswalkerTypes() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/planeswalker-types');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getLandTypes()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getLandTypes() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/land-types');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getArtifactTypes()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getArtifactTypes() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/artifact-types');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getEnchantmentTypes()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getEnchantmentTypes() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/enchantment-types');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getSpellTypes()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSpellTypes() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/spell-types');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getPowers()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPowers() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/powers');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getToughnesses()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getToughnesses() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/toughnesses');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getLoyalties()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getLoyalties() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/loyalties');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComScryfall\ApiComScryfallEndpointInterface::getWatermarks()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getWatermarks() : ApiComScryfallCatalog
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'catalog/watermarks');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComScryfallCatalog::class, $json->provideOne());
	}
	
}
